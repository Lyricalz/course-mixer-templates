<?php

class Home extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index($page = '', $sub = '') {
        $data['lang'] = 'en';
        $data['dir'] = 'ltr';
        $data['ratings'] = 3;

        if ($page == 'search') :
            $data['pageHeader'] = array(
                'Keyword:' => 'Lorem ipsum',
                'Location:' => 'London',
                'When:' => 'Full time'
            );
            $data['amount'] = 10;
            if ($_POST) :
                redirect('search');
            endif;
        elseif ($page == 'course') :
        elseif ($page == 'enquiry') :
            if ($sub == 'add') :
                // Process form form data
                if ($this->input->is_ajax_request()) :
                    // When it's added kill the page and return 'added' string this is for desktop only
                    die('added');
                else :
                    // Process form data then add it and redirect to enquiry page, this is for mobile/tablet
                    redirect('enquiry');
                endif;
            endif;
        endif;

        $data['content'] = empty($page) ? 'home' : $page;
        $this->load->view('index', $data);
    }

    function get_location($type = '') {
        $lat = $this->input->post('lat');
        $long = $this->input->post('long');

        //$lat = 51.532063199999996;
        //$long = -0.0840134;

        $url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$long&sensor=false";

        $json = json_decode(file_get_contents($url));

        for ($i = 0; $i < count($json->results); $i++) :
            for ($j = 0; $j < count($json->results[$i]->address_components); $j++) :
                if ($json->results[$i]->address_components[$j]->types == 'postal_code') :
                    $final[] = $json->results[$i]->address_components[$j]->long_name;
                endif;
            endfor;
        endfor;

        //echo $final;

        $final = array(
            'area' => $json->results[2]->address_components[0]->long_name,
            'area2' => $json->results[2]->address_components[0]->short_name
        );

        die(json_encode($final));
    }

    function load_more() {
        if ($this->input->is_ajax_request()) :
            $data['ratings'] = 2;
            $data['amount'] = 100;
            $data['offset'] = $this->input->post('offset');
            //$content = $this->load->view('code-templates/single-course-list', $data, TRUE);
            $final = '';
            for ($i = 0; $i < $data['amount']; $i++) :
                $final .= $this->load->view('code-templates/single-course-list', $data, TRUE); //$content;
            endfor;
            die($final);
        else :
            show_404();
        endif;
    }

    function returnView($page) {
        if ($this->input->is_ajax_request()) :
            $data = array();
            $final = $this->load->view('code-templates/' . $page, $data, TRUE);
            die($final);
        else :
            show_404();
        endif;
    }

    function processFileUpload($page) {
        $this->load->library('upload');
        if (empty($page)) :
            show_404();
        endif;
        if ($page == 'register') :
            $config['upload_path'] = 'uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '100';
            $config['max_width'] = '1024';
            $config['max_height'] = '768';
            $this->upload->initialize($config);

            if (!$this->upload->do_upload()) {
                $error = array('error' => $this->upload->display_errors());

                $this->load->view('upload_form', $error);
            } else {
                $data = array('upload_data' => $this->upload->data());

                $this->load->view('upload_success', $data);
            }
        endif;
    }

}