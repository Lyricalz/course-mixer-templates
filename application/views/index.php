<!DOCTYPE HTML>

<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="<?= $lang; ?>" dir="<?= $dir; ?>"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="<?= $lang; ?>" dir="<?= $dir; ?>"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="<?= $lang; ?>" dir="<?= $dir; ?>"> <![endif]-->
<!--[if gt IE 8]> <html class="no-js" lang="<?= $lang; ?>" dir="<?= $dir; ?>"> <![endif]-->
<html class="no-js" lang="<?= $lang; ?>" dir="<?= $dir; ?>" 
        data-intent
        data-in-mobile-class="mobile-view"
        data-in-tablet-class="tablet-view above-phone"
        data-in-standard-class="desktop-view above-phone"
        data-in-touch-class="touch-view">
    <head>
    <?php include 'includes/meta.php'; ?>
    </head>
    <body class="<?= empty($id) ? $content : "$content $id"; ?>">
        <?php include 'includes/header.php'; ?>
        <div id="content-area" class="container">
            <?php include 'includes/pageHeader.php'; ?>
            <div class="wrapper">
                <?php if ($content !== implode($this->config->item('sidebar-exclude'))) : ?> 
                <aside id="sidebar" class="">
                    <?php include 'includes/sidebar.php'; ?>
                </aside>
                <?php endif; ?>
                <div id="content">
                    <?php include 'pages/' . $content . '.php'; ?>
                </div>
            </div>
        </div>
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/footer-meta.php'; ?>
    </body>
</html>