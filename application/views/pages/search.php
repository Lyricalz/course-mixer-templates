<div class="search-options clearfix">
    <div class="search-results-count">
        <span>166 results</span>
    </div>
    <div class="search-subscribe">
        <a href="#">Subscribe for these results</a>
    </div>
    <div class="search-results-save">
        <a href="#">Save search results</a>
    </div>
</div>

<div class="sort-search">
    <label for="sort-select">Order By:</label>
    <select name="" id="sort-select" class="selectBox">
        <option value="ratings">Ratings</option>
        <option value="price">Price</option>
    </select>
</div>

<div class="profession-intro clearfix hidden-xs">
    <div class="profession-video">
        <a href="">
            <img src="<?= static_url(); ?>images/videoPreview.png" class="img-responsive" alt="">
            <div class="help-text">Watch this</div>
        </a>
    </div>
    <div class="profession-facts course-content">
        <h5>Some facts about this profession</h5>
        <p>
            Duis aute irure dolor in reprehenderit sunt in Duisirur Du 
            in voluptatecaecat cupidatat non proid irure dolor in rep
            ent, sunt in Duisirur Duis aute irure dolor in reprehe...
        </p>
        <div class="profession-avgSalary clearfix">
            <h5>Average Salary For This Profession</h5>
            <div class="local-avgSalary pull-left">
                <div class="help-text">
                    <div class="price">£ 1,350</div>
                    <div class="profession-location">United Kingdom</div>
                </div>
            </div>
            <div class="world-avgSalary pull-right">
                <div class="help-text">
                    <div class="price">£ 1,000</div>
                    <div class="profession-location">Worldwide</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="course-items" data-offset="0">
    <?php
    for ($i = 0; $i < 10; $i++) :
        echo $this->load->view('code-templates/single-course-list', '', TRUE);
    endfor;
    ?>
    <div class="load-more">
        <a href="#" class="sprite-loading"></a>
    </div>
</div>

