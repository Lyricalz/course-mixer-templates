<div class="course-wrap">
    <div class="course-overview clearfix">
        <div class="course-image">
            <form action="<?= base_url(); ?>enquiry/add" method="post" class="enquire" 
                  intent 
                  data-in-standard-after=".course-content"
                  data-in-mobile-prepend=".course-image"
                  data-in-tablet-after=".course-content"
                  >
                <input type="hidden" name="course" value="Lorem ipsum dolor sit na entermater iste" />
                <input type="hidden" name="course-id" value="1" />
                <button type="submit" class="visible-xs"><span class="text">Tap to add to enquiry list</span> <span class="sprite-plus"></span></button>
                <button type="submit" class="hidden-xs cm-button">Add to enquiry list</button>
            </form>
            <img src="<?= static_url(); ?>images/course.png" class="img-responsive">
            <!--<span class="image-fade"></span>-->
        </div>
        <div class="course-content-wrap">
            <div class="course-content">
                <h1 class="course-title">LOREM IPSUM DOLOR SIT NA ENTERMATER ISTE</h1>
                <div class="course-ratings" data-ratings="<?= $ratings; ?>">
                    <?php for ($j = 0; $j < $ratings; $j++) : ?>
                        <span class="sprite-star-big-active"></span>
                    <?php endfor; ?>
                    <?php for ($j = 0; $j < (5 - $ratings); $j++) : ?>
                        <span class="sprite-star-big"></span>
                    <?php endfor; ?>
                </div>
                <div class="course-price hidden-xs">Average Price:<span>£600.00</span> </div>
            </div>

        </div>

    </div>
    <a href="<?= base_url(); ?>provider" class="course-provider visible-xs">
        Provided by:
        <br/>
        <span>Training Dragon</span>
    </a>
    <div class="course-classes">
        <a href="#" class="next-classes">Show next classes</a>
        <div class="classes-wrap" data-count="7">
            <?php for ($i = 0; $i < 7; $i++) : ?>
                <div id="course-class-<?= $i; ?>" class="course-class<?= $i == 3 ? ' active' : ''; ?>">
                    <div class="course-class-wrap">
                        <div class="class-toggle-title">
                        <div class="course-class-meta clearfix" data-toggle="#course-class-text-<?= $i; ?>, #course-class-button-<?= $i; ?>">
                            <div class="from">
                                from
                                <br/>
                                <span class="course-date">Fri 17 Sept</span>
                                <br/>
                                11AM / 3PM
                            </div>
                            <div class="to">
                                to
                                <br/>
                                <span class="course-date">Mon 28 Sept</span>
                                <br/>
                                Every Monday
                            </div>
                        </div>
                        <div id="course-class-text-<?= $i; ?>" class="course-class-description<?= $i == 3 ? '' : ' hide'; ?>" data-toggle-target>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                Quisque euismod eget purus vitae auctor. 
                                Curabitur egestas felis laoreet augue sollicitudin, at tempor urna auctor.
                            </p>
                            <p class="course-price">£600.00</p>
                        </div>
                        </div>
                        <div class="class-toggle-content"></div>
                    </div>
                    <form action="<?= base_url(); ?>enquiry/add" method="post" class="enquire">
                        <input type="hidden" name="course" value="Lorem ipsum dolor sit na entermater iste" />
                        <input type="hidden" name="course-id" value="1" />
                        <input id="course-class-button-<?= $i; ?>" type="submit" value="Enquire" class="cm-button<?= $i == 3 ? '' : ' hide'; ?>" data-toggle-target />
                    </form>
                </div>
            <?php endfor; ?>
        </div>
        <a href="#" class="prev-classes">Show previous classes</a>
    </div>

    <div class="course-info-wrap">
        <div class="tab-toggle-wrap clearfix">
            <div class="tab-toggle-title">
            <div id="course-description" class="course-description toggle-parent">
                <a href="#" class="active" data-toggle="#course-description-text">Course Description <span class="sprite-dropdown-arrow"></span></a>
                <div id="course-description-text" class="tab-info course-info" data-toggle-target intent data-in-mobile-append="#course-description" data-in-tablet-append=".tab-toggle-content" data-in-standard-append=".tab-toggle-content">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        Nunc euismod, nisi vel laoreet semper, tellus elit imperdiet neque, ut lobortis justo lectus quis ipsum. 
                        In molestie erat quis lorem accumsan tincidunt. 
                        Fusce suscipit ornare nibh blandit rhoncus. 
                        Nullam vulputate porta quam eget dapibus. 
                        Ut mollis ornare felis et molestie. 
                        Etiam bibendum volutpat ligula, id eleifend est. 
                        Mauris luctus nulla tincidunt erat gravida, vel dapibus dolor imperdiet. 
                        Sed quam diam, iaculis eget pharetra id, placerat sed nisi.

                        Aenean lorem lacus, fermentum pulvinar risus pharetra, porta volutpat sapien. 
                        Sed eget est at tellus sodales feugiat eu nec mi. Suspendisse elementum nisl vitae ligula vestibulum, sit amet tempor sem fermentum. Maecenas in lectus eu tellus tempus consectetur venenatis non mi. Quisque ac magna eu nisl accumsan sollicitudin quis et turpis. Phasellus vehicula scelerisque viverra. Suspendisse ac arcu id nibh condimentum condimentum. Aliquam erat volutpat. Praesent nisl diam, lobortis vel vulputate lacinia, ultrices vitae neque. 
                        Nulla auctor est in porta blandit.
                    </p>
                    <a href="#" class="uppercase">Share on Socials</a>
                </div>
            </div>
            <div id="course-contents" class="course-contents toggle-parent">
                <a href="#" data-toggle="#course-contents-text">Course Contents<span class="sprite-dropdown-arrow"></span></a>
                <div id="course-contents-text" class="tab-info course-info hide" data-toggle-target intent data-in-mobile-append="#course-contents" data-in-tablet-append=".tab-toggle-content" data-in-standard-append=".tab-toggle-content">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        Nunc euismod, nisi vel laoreet semper, tellus elit imperdiet neque, ut lobortis justo lectus quis ipsum. 
                        In molestie erat quis lorem accumsan tincidunt. 
                        Fusce suscipit ornare nibh blandit rhoncus. 
                        Nullam vulputate porta quam eget dapibus. 
                        Ut mollis ornare felis et molestie. 
                        Etiam bibendum volutpat ligula, id eleifend est. 
                        Mauris luctus nulla tincidunt erat gravida, vel dapibus dolor imperdiet. 
                        Sed quam diam, iaculis eget pharetra id, placerat sed nisi.

                        Aenean lorem lacus, fermentum pulvinar risus pharetra, porta volutpat sapien. 
                        Sed eget est at tellus sodales feugiat eu nec mi. 
                        Suspendisse elementum nisl vitae ligula vestibulum, sit amet tempor sem fermentum. 
                        Maecenas in lectus eu tellus tempus consectetur venenatis non mi. 
                        Quisque ac magna eu nisl accumsan sollicitudin quis et turpis. 
                        Phasellus vehicula scelerisque viverra. 
                        Suspendisse ac arcu id nibh condimentum condimentum. 
                        Aliquam erat volutpat. 
                        Praesent nisl diam, lobortis vel vulputate lacinia, ultrices vitae neque. 
                        Nulla auctor est in porta blandit.
                    </p>
                </div>
            </div>
            <div id="course-outcomes" class="course-outcomes toggle-parent">
                <a href="#" data-toggle="#course-outcomes-text">Course Outcomes<span class="sprite-dropdown-arrow"></span></a>
                <div id="course-outcomes-text" class="tab-info course-info hide" data-toggle-target intent data-in-mobile-append="#course-outcomes" data-in-tablet-append=".tab-toggle-content" data-in-standard-append=".tab-toggle-content">
                    <ul>
                        <li>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        </li>
                        <li>
                            Nunc euismod, nisi vel laoreet semper, tellus elit imperdiet neque, ut lobortis justo lectus quis ipsum. 
                        </li>
                        <li>
                            In molestie erat quis lorem accumsan tincidunt. 
                        </li>
                        <li>
                            Fusce suscipit ornare nibh blandit rhoncus. 
                        </li>
                        <li>
                            Nullam vulputate porta quam eget dapibus. 
                        </li>
                        <li>
                            Ut mollis ornare felis et molestie. 
                        </li>
                        <li>
                            Etiam bibendum volutpat ligula, id eleifend est.
                        </li>
                        <li>
                            Mauris luctus nulla tincidunt erat gravida, vel dapibus dolor imperdiet. 
                        </li>
                        <li>
                            Sed quam diam, iaculis eget pharetra id, placerat sed nisi.
                        </li>
                        <li>
                            Aenean lorem lacus, fermentum pulvinar risus pharetra, porta volutpat sapien.
                        </li>
                        <li>
                            Sed eget est at tellus sodales feugiat eu nec mi. 
                        </li>
                        <li>
                            Suspendisse elementum nisl vitae ligula vestibulum, sit amet tempor sem fermentum. 
                        </li>
                        <li>
                            Maecenas in lectus eu tellus tempus consectetur venenatis non mi. 
                        </li>
                        <li>
                            Quisque ac magna eu nisl accumsan sollicitudin quis et turpis. 
                        </li>
                        <li>
                            Phasellus vehicula scelerisque viverra. 
                        </li>
                        <li>
                            Suspendisse ac arcu id nibh condimentum condimentum. 
                        </li>
                        Aliquam erat volutpat. Praesent nisl diam, lobortis vel vulputate lacinia, ultrices vitae neque. 
                        </li>
                        <li>
                            Nulla auctor est in porta blandit.
                        </li>
                    </ul>
                </div>
            </div>
            <div id="course-programme" class="course-programme toggle-parent">
                <a href="#" data-toggle="#course-programme-text">Course Programmes<span class="sprite-dropdown-arrow"></span></a>
                <div id="course-programme-text" class="tab-info course-info hide" data-toggle-target intent data-in-mobile-append="#course-programme" data-in-tablet-append=".tab-toggle-content" data-in-standard-append=".tab-toggle-content">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        Nunc euismod, nisi vel laoreet semper, tellus elit imperdiet neque, ut lobortis justo lectus quis ipsum. 
                        In molestie erat quis lorem accumsan tincidunt. 
                        Fusce suscipit ornare nibh blandit rhoncus. 
                        Nullam vulputate porta quam eget dapibus. 
                        Ut mollis ornare felis et molestie. 
                        Etiam bibendum volutpat ligula, id eleifend est. 
                        Mauris luctus nulla tincidunt erat gravida, vel dapibus dolor imperdiet. 
                        Sed quam diam, iaculis eget pharetra id, placerat sed nisi.

                        Aenean lorem lacus, fermentum pulvinar risus pharetra, porta volutpat sapien. 
                        Sed eget est at tellus sodales feugiat eu nec mi. Suspendisse elementum nisl vitae ligula vestibulum, sit amet tempor sem fermentum. Maecenas in lectus eu tellus tempus consectetur venenatis non mi. Quisque ac magna eu nisl accumsan sollicitudin quis et turpis. Phasellus vehicula scelerisque viverra. Suspendisse ac arcu id nibh condimentum condimentum. Aliquam erat volutpat. Praesent nisl diam, lobortis vel vulputate lacinia, ultrices vitae neque. 
                        Nulla auctor est in porta blandit.
                    </p>
                </div>
            </div>
            <div id="course-location" class="course-location location-tab toggle-parent">
                <a href="#" data-toggle="#course-location-text">Location<span class="sprite-dropdown-arrow"></span></a>
                <div id="course-location-text" class="tab-info course-info hide" data-toggle-target intent data-in-mobile-append="#course-location" data-in-tablet-append=".tab-toggle-content" data-in-standard-append=".tab-toggle-content">
                    <?php $this->load->view('code-templates/location-tab'); ?>
                </div>
            </div>
            <div id="course-contact" class="course-contact contact-tab toggle-parent">
                <a href="#" data-toggle="#course-contact-text">Contact<span class="sprite-dropdown-arrow"></span></a>
                <div id="course-contact-text" class="tab-info course-info hide" data-toggle-target intent data-in-mobile-append="#course-contact" data-in-tablet-append=".tab-toggle-content" data-in-standard-append=".tab-toggle-content">
                    <div class="contact-form">
                        <form>
                        <?php $this->load->view('code-templates/contact-form'); ?>
                        </form>
                        <p class="help-text">Or</p>
                        <a href="mailto:someone@somewhere.com" class="send-mail"><span class="sprite-mail"></span>Send an email</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-toggle-content"></div>
        </div>
        <div id="course-ratings" class="course-ratings ratings-tab clearfix toggle-parent">
            <div class="text pull-left">User Ratings</div>
            <div class="course-rate pull-right">
                Overall:
                <br/>
                <?php for ($j = 0; $j < (5 - $ratings); $j++) : ?>
                    <span class="sprite-star"></span>
                <?php endfor; ?>
                <?php for ($j = 0; $j < $ratings; $j++) : ?>
                    <span class="sprite-star-active"></span>
                <?php endfor; ?>

            </div>
        </div>

        <div id="course-reviews" class="course-reviews reviews-tab toggle-parent">
            <a href="#" data-toggle="#course-reviews-text">Reviews (2)<span class="sprite-dropdown-arrow"></span></a>
            <div id="course-reviews-text" class="tab-info course-info hide" data-toggle-target>
                <?php $this->load->view('code-templates/review-tab.php'); ?>
            </div>
        </div>

    </div>
</div>