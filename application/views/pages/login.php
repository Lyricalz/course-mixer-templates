<h1 class="h2">Login</h1>

<p class="help-text">
    Lorem ipsum dolor sit amet consectetur adipsendba eit.
</p>
<div class="login-wrap clearfix">
    <div class="content-a">
        <form class="clearfix">
            <div class="form-group">
                <label for="login-email" class="cm-label">Email:<span>Not public</span></label>
                <input type="email" name="email" id="login-email" class="form-control cm-input">
            </div>
            <div class="form-group">
                <label for="password" class="cm-label">Password:</label>
                <input type="password" name="password" id="password" class="form-control cm-input">
            </div>
            <a href="<?= base_url(); ?>register" class="help-text pull-left">Not Registered Yet?</a>
            <a href="<?= base_url(); ?>forgot" class="help-text pull-right">Forgot your password?</a>
            <button type="submit" class="cm-button cm-button-alt cm-button-lg">Login</button>
        </form>
    </div>

    <div class="content-b pull-right">
        <div class="social-login">
            <a href="#" class="cm-button cm-button-alt cm-button-lg twitter-login"><span class="sprite-twitter-soial"></span>Login with Twitter</a>
            <a href="#" class="cm-button cm-button-alt cm-button-lg facebook-login"><span class="sprite-twitter-soial"></span>Login with Facebook</a>
        </div>
    </div>
</div>