<div class="register-wrap clearfix">
    <form enctype="multipart/form-data" class="clearfix">
        <div class="content-a">
            <h1 class="h2">Create an account</h1>

            <p class="help-text">
                Lorem ipsum dolor sit amet consectetur adipsendba eit.
            </p>


            <div class="form-group">
                <label for="title" class="cm-label">Title:</label>
                <select name="title" id="title" class="cm-input selectBox">
                    <option value=""></option>
                    <option value="mr">Mr.</option>
                    <option value="ms">Ms.</option>
                    <option value="mrs">Mrs.</option>
                </select>
            </div>
            <div class="form-group">
                <label for="fname" class="cm-label">First Name</label>
                <input type="text" name="fname" id="fname" class="form-control cm-input">
            </div>
            <div class="form-group">
                <label for="lname" class="cm-label">Last Name</label>
                <input type="text" name="lname" id="lname" class="form-control cm-input">
            </div>
            <div class="form-group">
                <label for="email" class="cm-label">Email</label>
                <input type="email" name="email" id="email" class="form-control cm-input">
            </div>
            <div class="form-group">
                <label for="password" class="cm-label">Password</label>
                <input type="password" name="password" id="password" class="form-control cm-input">
            </div>
            <div class="form-group">
                <label for="password2" class="cm-label">Retype Password</label>
                <input type="password" name="password2" id="password2" class="form-control cm-input">
            </div>
        </div>

        <div class="content-b pull-right">
            <div class="about-you">
                <h2>About you</h2>
                <p class="help-text">
                    Lorem ipsum dolor sit amet consectetur adipsendba eit.
                </p>
                <div class="upload-wrap clearfix">
                    <div class="upload-preview pull-left">
                        <div class="preview"></div>
                    </div>
                    <label for="photo" class="cm-button cm-button-alt">Browse</label>
                    <input type="file" name="photo" id="photo" class="">
                </div>

                <div class="form-group">
                    <label for="yourself">Something about yourself</label>
                    <textarea name="yourself" id="yourself" class="form-control cm-input"></textarea>
                </div>
                <p class="help-text">
                    By clicking sign up I agree to the <a href="#">terms and conditions</a>.
                </p>

            </div>
            <input type="submit" class="cm-button cm-button-alt cm-button-lg" value="Submit">

        </div>
    </form>
</div>