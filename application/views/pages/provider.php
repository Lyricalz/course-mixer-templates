<h1>Provider Name</h1>
<div class="tab-toggle-wrap clearfix">
    <div class="tab-toggle-title">
        <div id="provider-description" class="provider-description toggle-parent">
            <a href="#" data-toggle="#provider-description-text">Provider Description<span class="sprite-dropdown-arrow"></span></a>
            <div id="provider-description-text" class="tab-info provider-info" data-toggle-target intent data-in-mobile-append="#provider-description" data-in-tablet-append=".tab-toggle-content" data-in-standard-append=".tab-toggle-content">
                <div class="provider-image">
                    <img src="<?= static_url(); ?>images/provider.png" class="img-responsive" alt="">
                </div>
                <div class="provider-text">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        Nunc euismod, nisi vel laoreet semper, tellus elit imperdiet neque, ut lobortis justo lectus quis ipsum. 
                        In molestie erat quis lorem accumsan tincidunt. 
                        Fusce suscipit ornare nibh blandit rhoncus. 
                        Nullam vulputate porta quam eget dapibus. 
                        Ut mollis ornare felis et molestie. 
                        Etiam bibendum volutpat ligula, id eleifend est. 
                        Mauris luctus nulla tincidunt erat gravida, vel dapibus dolor imperdiet. 
                        Sed quam diam, iaculis eget pharetra id, placerat sed nisi.
                    </p>
                    <a href="#" class="uppercase">Visit Website</a>
                    <br/>
                    <a href="#" class="uppercase">Share on Socials</a>
                </div>
            </div>
        </div>
        <div id="provider-tutors" class="provider-tutors toggle-parent">
            <a href="#" data-toggle="#provider-tutors-text">Provider Tutors<span class="sprite-dropdown-arrow"></span></a>
            <div id="provider-tutors-text" class="tab-info provider-info hide" data-toggle-target intent data-in-mobile-append="#provider-tutor" data-in-tablet-append=".tab-toggle-content" data-in-standard-append=".tab-toggle-content">
                <div class="provider-text">
                    <h4>Tutor Name 1</h4>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        Nunc euismod, nisi vel laoreet semper, tellus elit imperdiet neque, ut lobortis justo lectus quis ipsum. 
                        In molestie erat quis lorem accumsan tincidunt. 
                        Fusce suscipit ornare nibh blandit rhoncus. 
                        Nullam vulputate porta quam eget dapibus. 
                        Ut mollis ornare felis et molestie. 
                        Etiam bibendum volutpat ligula, id eleifend est. 
                        Mauris luctus nulla tincidunt erat gravida, vel dapibus dolor imperdiet. 
                        Sed quam diam, iaculis eget pharetra id, placerat sed nisi.
                    </p>

                    <h4>Tutor Name 2</h4>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        Nunc euismod, nisi vel laoreet semper, tellus elit imperdiet neque, ut lobortis justo lectus quis ipsum. 
                        In molestie erat quis lorem accumsan tincidunt. 
                        Fusce suscipit ornare nibh blandit rhoncus. 
                        Nullam vulputate porta quam eget dapibus. 
                        Ut mollis ornare felis et molestie. 
                        Etiam bibendum volutpat ligula, id eleifend est. 
                        Mauris luctus nulla tincidunt erat gravida, vel dapibus dolor imperdiet. 
                        Sed quam diam, iaculis eget pharetra id, placerat sed nisi.
                    </p>
                </div>
            </div>
        </div>

        <div id="provider-location" class="provider-location location-tab toggle-parent">
            <a href="#" data-toggle="#provider-location-text">Location<span class="sprite-dropdown-arrow"></span></a>
            <div id="provider-location-text" class="tab-info provider-info hide" data-toggle-target intent data-in-mobile-append="#provider-location" data-in-tablet-append=".tab-toggle-content" data-in-standard-append=".tab-toggle-content">
                <?php $this->load->view('code-templates/location-tab'); ?>
            </div>
        </div>

        <div id="provider-contact" class="provider-contact contact-tab toggle-parent">
            <a href="#" data-toggle="#provider-contact-text">Contact<span class="sprite-dropdown-arrow"></span></a>
            <div id="provider-contact-text" class="tab-info provider-info hide" data-toggle-target intent data-in-mobile-append="#provider-contact" data-in-tablet-append=".tab-toggle-content" data-in-standard-append=".tab-toggle-content">
                <div class="contact-form">
                    <?php $this->load->view('code-templates/contact-form'); ?>
                    <p class="help-text">Or</p>
                    <a href="mailto:someone@somewhere.com" class="send-mail"><span class="sprite-mail"></span>Send an email</a>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-toggle-content"></div>
</div>

<div class="course-offered">
    Courses Offered By This Provider (35)
</div>

<div class="course-items" data-offset="0">
    <?php $final = $this->load->view('code-templates/single-course-list', '', TRUE); ?>
    <?php
    for ($i = 0; $i < 5; $i++) :
        echo $final;
    endfor;
    ?>
    <div class="load-more">
        <a href="#" class="sprite-loading"></a>
    </div>
</div>