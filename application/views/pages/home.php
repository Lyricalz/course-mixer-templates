<div class="home-intro hidden-xs">
    <img src="<?= static_url(); ?>images/bigLogo.png">

    <div class="random-img" style="background-image: url(<?= static_url(); ?>images/home/<?= rand(1, 2); ?>.png)"></div>
</div>

<div class="home-form">
    <form role="form" action="<?= base_url(); ?>search" method="post" class="clearfix">
        <div class="form-group">
            <label for="course" class="cm-label">I am interested in learning</label>
            <input type="text" id="course" class="course-typeahead form-control cm-input" autocomplete="off">
        </div>
        <div class="form-group location">
            <label for="courseLocation" class="cm-label">In</label>
            <a href="#" id="courseLocationButton" class="sprite-find-me"></a>
            <input type="text" class="form-control cm-input" id="courseLocation" placeholder="Postcode or town">
        </div>
        <input type="hidden" id="location-hidden">
        <input type="submit" class="cm-button" value="search">
    </form>
</div>