<script>
    var content = '<?= $content; ?>';
    var base_url = '<?= base_url(); ?>';
    var static_url = '<?= static_url(); ?>';
</script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<?php if ($content !== implode($this->config->item('js-exclude'))) : ?>
    <script type="text/javascript" src="<?= static_url() . getVer('js/min/plugins.min.js'); ?>"></script>
    <script type="text/javascript" src="<?= static_url() . getVer('js/min/default.min.js'); ?>"></script>
<?php endif; ?>