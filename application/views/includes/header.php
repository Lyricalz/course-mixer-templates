<header id="header" class="container navbar-fixed-top">
    <a href="#" class="course-nav-button left"><span class="sprite-left-arrow"></span></a>
    <a href="#" class="course-nav-button right"><span class="sprite-right-arrow"></span></a>
    <nav class="navbar navbar-default visible-xs" role="navigation">
        <div class="navbar-header-wrap visible-xs">
            <div class="navbar-header">
                <div class="logo"><a href="<?= base_url(); ?>"><img src="<?= static_url(); ?>images/logo.png" alt=""/></a></div>
                <button type="button" class="navbar-toggle search-toggle collapsed" data-toggle="collapse" data-target=".search-menu">
                    <span class="sr-only">Toggle search navigation</span>
                    <span class="sprite-search-active"></span>
                </button>
                <button type="button" class="navbar-toggle menu-toggle collapsed" data-toggle="collapse" data-target=".navbar-menu">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-menu">
            <ul class="nav navbar-nav">
                <li>
                    <div class="menu-programme toggle-parent">
                        <a href="#" data-toggle=".menu-programme">
                            <span class="menu-title">Programs</span>
                            <span class="menu-subtitle">Find programs by subject area</span>
                            <span class="sprite-dropdown-arrow"></span>
                        </a>
                        <div class="course-info hide" data-toggle-target>
                            <input type="text" class="form-control cm-input searchText">
                        </div>
                    </div>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
        <div class="collapse navbar-collapse search-menu">
            <ul class="nav navbar-nav">
                <li>
                    <div class="search-keyword toggle-parent">
                        <a href="#" data-toggle="nav" data-toggle-target="#search-keyword-wrap">
                            <span class="search-key">Keyword:</span>
                            <span class="search-val">Design</span>
                            <span class="sprite-dropdown-arrow"></span>
                        </a>
                        <div id="search-keyword-wrap" class="course-info hide">
                            <input type="text" class="form-control cm-input searchText">
                        </div>
                    </div>
                </li>
                <li>
                    <?php $this->load->view('code-templates/search-nav/location'); ?>
                </li>
                <li>
                    <?php $this->load->view('code-templates/search-nav/when-how'); ?>
                </li>
                <li>
                    <?php $this->load->view('code-templates/search-nav/price'); ?>
                </li>
                <li>
                    <?php $this->load->view('code-templates/search-nav/category'); ?>
                </li>
            </ul>
        </div>
    </nav>
</header>