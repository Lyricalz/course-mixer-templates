<?php if (!empty($pageHeader) && $content == 'search') : ?>
    <div class="pageHeader visible-xs">
        <?php if (is_array($pageHeader)) : ?>
            <?php foreach ($pageHeader as $key => $val) : ?>
                <span class="search-key"><?= $key; ?></span>
                <span class="search-val"><?= $val; ?></span>
                <br/>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
<?php endif; ?>

<?php if ($content == 'course') : ?>
    <div class="pageHeader">
        <a href="<?= base_url(); ?>search">
            <span class="sprite-left-arrow-hover"></span>
            <span class="course-back">Back to Search Results</span>
        </a>
    </div>
<?php endif; ?>

