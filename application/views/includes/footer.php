<footer id="footer" class="<?= $content == 'home' ? 'navbar-fixed-bottom ' : ''; ?>">
    <div class="wrapper">
        <?php if ($content != 'home') : ?>
            <div id="suggestion-box">
                <img src="<?= static_url(); ?>images/suggestion.png" alt="" />
                <a href="#">Suggestion box</a>
            </div>
            <ul id="social-links" class="clearfix">
                <li><a href="#" class="sprite-twitter-social">Twitter</a></li>
                <li><a href="#" class="sprite-facebook-social">Facebook</a></li>
                <li><a href="#" class="sprite-pinterest-social">Pinterest</a></li>
                <li><a href="#" class="sprite-gplus-social">Google+</a></li>
            </ul>
        <?php endif; ?>
            <?php
            if (!in_array($content, $this->config->item('enquiry-exclude'))) :
                $this->load->view('code-templates/enquiry'); ?>
        <div class="enquiry-footer clearfix" data-intent data-in-mobile-class="navbar-fixed-bottom" data-in-tablet-class="navbar-fixed-bottom" data-in-standard-class="hide">
           <!-- <a href="<?= base_url(); ?>enquiry" data-intent data-in-tablet-class="center">Enquiry List</a>-->
            
        </div>
            <?php endif; ?>
    </div>
    <div id="footer-meta" class="">
        <div class="desktop-link visible-xs">
        <a href="#">Go to desktop version</a>
        </div>
    </div>
</footer>