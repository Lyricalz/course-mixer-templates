<div class="course-item clearfix course-item-">
    <div class="course-image hidden-xs">
        <a href="<?= base_url(); ?>course">
            <img src="<?= static_url(); ?>images/course/<?= rand(1,3); ?>.png" class="img-responsive" alt="">
        </a>
    </div>
    <div class="course-content-wrap">
        <div class="course-content">
            <a href="<?= base_url(); ?>course" class="sprite-right-arrow-hover"></a>
            <h2><a href="<?= base_url(); ?>course">Lorem ipsum dolor sit na entermater iste</a></h2>
            <p class="course-description hidden-xs">
                Duis aute irure dolor in reprehenderit in volupta
                caecat cupidatat non proident, sunt in Duisirur...
            </p>
            <div class="course-meta clearfix">
                <div class="course-price">Price:<span>£600.00</span> </div>
                <div class="course-ratings" data-ratings="<?= $ratings; ?>">
                    <?php for ($j = 0; $j < $ratings; $j++) : ?>
                        <span class="sprite-star-active"></span>
                    <?php endfor; ?>
                    <?php for ($j = 0; $j < (5 - $ratings); $j++) : ?>
                        <span class="sprite-star"></span>
                    <?php endfor; ?>
                </div>
            </div>
            <div class="course-provider clearfix">
                <div class="course-provider-name">
                    Provided By:
                    <br/>
                    <a href="<?= base_url(); ?>provider">Training Dragon</a>
                </div>
                <div class="course-provider-location">
                    <a href="#" class="sprite-find-me-grey hidden-xs"></a>
                    <address>
                        75, Kings Cross Road<br/>SW21JE<br/>LONDON
                    </address>
                </div>
            </div>
        </div>
        <form action="<?= base_url(); ?>enquiry/add" method="post" class="enquire">
            <input type="hidden" name="course" value="Lorem ipsum dolor sit na entermater iste" />
            <input type="hidden" name="course-id" value="1" />
            <button type="submit" class="cm-button">Enquire Now</button>
            <!--<div class="enquire-animation"></div>-->
        </form>
    </div>
</div>