<div class="course-class">
    <div class="course-class-wrap">
        <div class="course-class-meta clearfix" data-toggle=".course-class">
            <div class="from">
                from
                <br/>
                <span class="course-date">Fri 17 Sept</span>
                <br/>
                11AM / 3PM
            </div>
            <div class="to">
                to
                <br/>
                <span class="course-date">Mon 28 Sept</span>
                <br/>
                Every Monday
            </div>
        </div>
        <div class="course-class-description hide" data-toggle-target>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                Quisque euismod eget purus vitae auctor. 
                Curabitur egestas felis laoreet augue sollicitudin, at tempor urna auctor.
            </p>
            <p class="course-price">£600.00</p>
        </div>
    </div>
    <form action="<?= base_url(); ?>enquiry/add" method="post" class="enquire">
        <input type="hidden" name="course" value="Lorem ipsum dolor sit na entermater iste" />
        <input type="hidden" name="course-id" value="1" />
        <input type="submit" value="Enquire" class="cm-button hide" data-toggle-target />
    </form>
</div>