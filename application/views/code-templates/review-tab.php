<div class="review-sort">
    <label for="sort-select">Sort By:</label>
    <select name="" id="sort-select" class="selectBox">
        <option value="recent">Most recent</option>
        <option valuue="ratings">Most ratings</option>
        <option valuue="date">Latest</option>
    </select>
</div>
<ol>
    <?php for ($i = 0; $i < 5; $i++) : ?>
        <li class="review-entry">
            <div class="review-heading"><?= $i + 1; ?> - Lorem ipsum dolor sit</div>
            <div class="review-ratings">
                <?php for ($j = 0; $j < $ratings; $j++) : ?>
                    <span class="sprite-star-lblue-active"></span>
                <?php endfor; ?>
                <?php for ($j = 0; $j < (5 - $ratings); $j++) : ?>
                    <span class="sprite-star-lblue"></span>
                <?php endfor; ?>
            </div>
            <div class="review-author">
                Lorem ipsum
            </div>
            <div class="review-text">
                <p>
                    Aenean lorem lacus, fermentum pulvinar risus pharetra, porta volutpat sapien. 
                    Sed eget est at tellus sodales feugiat eu nec mi. Suspendisse elementum nisl vitae ligula vestibulum, sit amet tempor sem fermentum. Maecenas in lectus eu tellus tempus consectetur venenatis non mi. Quisque ac magna eu nisl accumsan sollicitudin quis et turpis. Phasellus vehicula scelerisque viverra. Suspendisse ac arcu id nibh condimentum condimentum. Aliquam erat volutpat. Praesent nisl diam, lobortis vel vulputate lacinia, ultrices vitae neque. 
                    Nulla auctor est in porta blandit.
                </p>
            </div>
        </li>
    <?php endfor; ?>
</ol>
<div class="review-help">
    <strong>Have you attended this class?</strong>
    <p>
        Got something to say about this class?
        <br/>
        Leave a review and tell us what you think.
    </p>
</div>
<div class="review-form">
    <form>
        <label for="review-input">Write your review</label>
        <input type="text" name="name2" class="hide" />
        <textarea name="review" id="review-input" class="form-control cm-input"></textarea>
        <div class="review-rate">
            Rate this class: 
            <div class="review-stars-wrap">
                <?php for ($i = 0; $i < 5; $i++) : ?>
                    <span class="sprite-star-lblue"></span>
                <?php endfor; ?>
            </div>
        </div>
        <input type="submit" class="cm-button cm-button" value="Add Review">
    </form>
</div>