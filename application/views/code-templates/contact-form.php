<div class="form-group">
    <label for="name" class="cm-label">Name:</label>
    <input type="text" name="name" id="name" class="form-control cm-input">
</div>
<div class="form-group">
    <label for="email" class="cm-label">Email:</label>
    <input type="email" name="email" id="email" class="form-control cm-input">
</div>
<div class="form-group">
    <label for="tel" class="cm-label">Tel:</label>
    <input type="tel" name="tel" id="tel" class="form-control cm-input">
</div>
<div class="form-group textarea">
    <label for="message" class="cm-label">Your message:</label>
    <textarea name="message" id="message" class="form-control cm-input"></textarea>
</div>
<div class="checkbox">
    <input type="checkbox" name="send-copy" id="send-copy" class="css-checkbox" value="">
    <label for="send-copy" class="cm-label css-label small">
        Please send a copy to my email address
    </label>
</div>
<?php if ($content == 'enquiry') : ?>
    <input type="submit" value="Enquire all" class="cm-button cm-button-alt">
<?php else : ?>
    <input type="submit" value="Send" class="cm-button cm-button-alt">
<?php endif; ?>