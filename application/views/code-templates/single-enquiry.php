<li>
    <input type="checkbox" name="enquiry[]" id="enquiry<?= $i; ?>" class="css-checkbox" value="<?= $i; ?>" <?= $i % 2 == 0 ? 'checked' : ''; ?>>
    <label for="enquiry<?= $i; ?>" class="cm-label css-label">
        <span>
            <span class="enquiry-name">Web Design Beginner</span>
            <span class="enquiry-provider">Dotpeak</span>
        </span>
    </label>
</li>