<div class="enquiry-wrap toggle-parent active" data-intent data-in-mobile-append=".enquiry-footer" data-in-tablet-append=".enquiry-footer" data-in-standard-append="#sidebar">
    <form>
        <a href="#" class="enquiry-head" data-toggle=".enquiry-body">Enquiry List<span class="sprite-dropdown-arrow-white"></span></a>
        <div class="enquiry-body hide" data-toggle-target>

            <div class="enquiry-list-wrap">
                <ul id="enquiry-list" class="enquiry-list checkAllParent"  data-count="">
                    <?php for ($i = 1; $i < 30; $i++) : ?>
                        <li>
                            <input type="checkbox" name="enquiry[]" id="enquiry<?= $i; ?>" class="css-checkbox" value="<?= $i; ?>" <?= $i % 2 == 0 ? 'checked' : ''; ?>>
                            <label for="enquiry<?= $i; ?>" class="cm-label css-label">
                                <span>
                                    <span class="enquiry-name">Web Design Beginner</span>
                                    <span class="enquiry-provider">Dotpeak</span>
                                </span>
                            </label>
                        </li>
                    <?php endfor; ?>
                </ul>
                <input type="checkbox" id="checkAll" class="css-checkbox" value="">
                <label for="checkAll" class="cm-label css-label checkAllLabel">Select All</label>
            </div>

            <div class="enquiry-contact">
                <div class="one-email clearfix">
                    <div class="sprite-mail"></div>
                    <div class="help-text">
                        <h4 class="uppercase">One Email To All</h4>
                        Contact all the providers at once
                    </div>
                </div>

                <?php $this->load->view('code-templates/contact-form'); ?>
            </div>
            
        </div>
    </form>
</div>