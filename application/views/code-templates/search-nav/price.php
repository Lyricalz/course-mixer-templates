<div class="search-price toggle-parent">
    <a href="#" data-toggle="nav" data-toggle-target="#search-price-wrap">
        <span class="search-key">Price:</span>
        <span class="search-val">£500 - £1000</span>
        <span class="sprite-dropdown-arrow"></span>
    </a>
    <div id="search-price-wrap" class="course-info hide">
        <select name="search-price" id="search-price" class="searchText">
            <option value="£0 - £500">£0 - £500</option>
            <option value="£500 - £1000">£500 - £1000</option>
            <option value="£1000 - £1500">£1000 - £1500</option>
            <option value="£1500+">£1500+</option>
        </select>
    </div>
</div>