<div class="search-when toggle-parent">
    <a href="#" data-toggle="nav" data-toggle-target="#search-when-wrap">
        <span class="search-key">When:</span>
        <span class="search-val">
            <div class="when-avail">(Full Time, Part Time)</div>
            <div class="when-date">10 Jan 2013 - 20 Jan 2013</div>
        </span>
        <span class="sprite-dropdown-arrow"></span>
    </a>
    <div id="search-when-wrap" class="course-info hide" data-toggle-target>
        <div class="search-how">
            <input type="checkbox" name="search-when[]" id="search-full" class="css-checkbox" value="full-time" checked>
            <label for="search-full" class="cm-label css-label">Full time</label>

            <input type="checkbox" name="search-when[]" id="search-part" class="css-checkbox" value="part-time" checked>
            <label for="search-part" class="cm-label css-label">Part time</label>

            <input type="checkbox" name="search-when[]" id="search-weekend" class="css-checkbox" value="weekend">
            <label for="search-weekend" class="cm-label css-label">Weekend</label>

            <input type="checkbox" name="search-when[]" id="search-short" class="css-checkbox" value="short">
            <label for="search-short" class="cm-label css-label">Short (Less than 7 days)</label>

            <input type="checkbox" name="search-when[]" id="search-customized" class="css-checkbox" value="customized">
            <label for="search-customized" class="cm-label css-label">Customized</label>

            <input type="checkbox" name="search-when[]" id="search-online" class="css-checkbox" value="online">
            <label for="search-online" class="cm-label css-label">Online</label>
        </div>
        <div class="search-date">
            <div class="search-date-header">Or select by dates</div>
            <div class="search-date-body">
                <select name="day-from" id="day-from">
                    <?php for ($i = 1; $i < 32; $i++) : ?>
                        <option value="<?= $i; ?>"><?= $i; ?></option>
                    <?php endfor; ?>
                </select>
                <select name="month-from" id="month-from">
                    <option>January</option>
                </select>
                <select name="year-from" id="year-from">
                    <option>2013</option>
                </select>
                
                <select name="day-to" id="day-to">
                    <?php for ($i = 1; $i < 32; $i++) : ?>
                        <option value="<?= $i; ?>"><?= $i; ?></option>
                    <?php endfor; ?>
                </select>
                <select name="month-to" id="month-to">
                    <option>January</option>
                </select>
                <select name="year-to" id="year-to">
                    <option>2013</option>
                </select>
            </div>
        </div>
    </div>
</div>