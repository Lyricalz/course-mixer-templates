<div class="search-category toggle-parent">
    <a href="#" data-toggle="nav" data-toggle-target="#search-category-wrap">
        <span class="search-key">Category:</span>
        <span class="search-val">Web Design</span>
        <span class="sprite-dropdown-arrow"></span>
    </a>
    <div id="search-category-wrap" class="course-info hide" >
        <select name="search-category" id="search-category" class="searchText">
            <option value="Web Design">Web Design</option>
            <option value="Graphic Design">Graphic Design</option>
            <option value="Web Development">Web Development</option>
        </select>
    </div>
</div>