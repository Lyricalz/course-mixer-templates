<div class="search-location toggle-parent">
    <a href="#" data-toggle="nav" data-toggle-target="#search-location-wrap">
        <span class="search-key">Location:</span>
        <span class="search-val">SW2 1JE</span>
        <span class="sprite-dropdown-arrow"></span>
    </a>
    <div id="search-location-wrap" class="course-info hide">
        <a href="#" id="courseLocationButton" class="sprite-find-me"></a>
        <input type="text" id="search-location" class="form-control cm-input searchText">
        <input type="hidden" id="location-hidden">
    </div>
</div>