<?php

function static_url() {
    $CI = & get_instance();
    return $CI->config->item('static_url');
}

function getVer($url) {
    if (ENVIRONMENT == 'local-dev') :
        return $url;
    endif;

    $file = $_SERVER['DOCUMENT_ROOT'] . '/static/course-mixer2/assets/' . $url;
    $final = filemtime($file);
    if (substr($url, -3) == 'css') :
        return str_replace('.css', '-' . $final . '.css', $url);
    elseif (substr($url, -2) == 'js') :
        return str_replace('.js', '-' . $final . '.js', $url);
    endif;
    
}